<?php

/**
 * @file
 * uw_ct_site_footer_logos.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_site_footer_logos_taxonomy_default_vocabularies() {
  return array(
    'uw_site_footer_logo_areas' => array(
      'name' => 'Site footer logo areas',
      'machine_name' => 'uw_site_footer_logo_areas',
      'description' => 'Areas used for the site footer logos',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'base_i18n_mode' => 0,
      'base_language' => 'und',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
