<?php

/**
 * @file
 * Site footer logos plugin definition.
 */

$plugin = array(
  'label' => t('Site footer logos'),
  'resource' => 'site_footer_logos',
  'name' => 'site_footer_logos__1_0',
  'entity_type' => 'node',
  'bundle' => 'uw_ct_site_footer_logo',
  'description' => t('Export the site footer logos.'),
  'class' => 'RestfulSiteFooterLogosResource',
);
