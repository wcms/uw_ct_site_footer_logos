<?php

/**
 * @file
 * Contains RestfulSiteFooterLogosResource__1_0.
 */

/**
 * Implements RestfulEntityBaseNode class for the "site footer logos".
 */
class RestfulSiteFooterLogosResource extends RestfulEntityBaseNode {

  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {

    $public_fields = parent::publicFieldsInfo();

    $public_fields['logo'] = array(
      'property' => 'field_uw_site_footer_logo',
      'process_callbacks' => array(
        array($this, 'getLogoInfo'),
      ),
    );

    $public_fields['weight'] = array(
      'property' => 'field_uw_site_footer_area',
      'sub_property' => 'weight',
    );

    $public_fields['alt'] = array(
      'property' => 'field_uw_site_footer_logo',
      'sub_property' => 'alt',
    );

    $public_fields['link'] = array(
      'property' => 'field_uw_site_footer_link',
      'sub_property' => 'url',
    );

    $public_fields['area'] = array(
      'property' => 'field_uw_site_footer_area',
      'sub_property' => 'name',
    );

    $public_fields['show_area'] = array(
      'property' => 'field_uw_site_footer_area',
      'sub_property' => 'field_uw_show_sf_selection',
    );

    return $public_fields;
  }

  /**
   * Gets URL for the logo.
   *
   * @param array $image
   *   Loaded image object in array form.
   *
   * @return string
   *   Image URL.
   */
  public function getLogoInfo(array $image) {

    // Get the file url from the file uri.
    $file_url = file_create_url($image['uri']);

    return $file_url;
  }

}
