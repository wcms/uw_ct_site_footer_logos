<?php

/**
 * @file
 * uw_ct_site_footer_logos.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ct_site_footer_logos_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-uw_ct_site_footer_logo-field_uw_site_footer_area'.
  $field_instances['node-uw_ct_site_footer_logo-field_uw_site_footer_area'] = array(
    'bundle' => 'uw_ct_site_footer_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the faulty, department or area that the site footer belongs to.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_plain',
        'weight' => 5,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_site_footer_area',
    'label' => 'Faculty/Department/Area',
    'required' => 1,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'options_select',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'node-uw_ct_site_footer_logo-field_uw_site_footer_link'.
  $field_instances['node-uw_ct_site_footer_logo-field_uw_site_footer_link'] = array(
    'bundle' => 'uw_ct_site_footer_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter the link that the logo will go to.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_url',
        'weight' => 4,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_site_footer_link',
    'label' => 'link',
    'required' => 1,
    'settings' => array(
      'absolute_url' => 1,
      'attributes' => array(
        'class' => '',
        'configurable_class' => 0,
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'entity_translation_sync' => FALSE,
      'rel_remove' => 'default',
      'title' => 'optional',
      'title_allowed_values' => '',
      'title_label_use_field_label' => 0,
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(
        'label_help_description' => '',
      ),
      'type' => 'link_field',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'node-uw_ct_site_footer_logo-field_uw_site_footer_logo'.
  $field_instances['node-uw_ct_site_footer_logo-field_uw_site_footer_logo'] = array(
    'bundle' => 'uw_ct_site_footer_logo',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_uw_site_footer_logo',
    'label' => 'Logo',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'alt_field_required' => 1,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => '',
      'file_extensions' => 'png',
      'focal_point' => 0,
      'focal_point_preview' => 0,
      'focal_point_styles' => array(
        'image_gallery_small' => 0,
        'image_gallery_squares' => 0,
        'image_gallery_standard' => 0,
        'image_gallery_wide' => 0,
      ),
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '20 KB',
      'max_resolution' => '400x400',
      'min_resolution' => '10x10',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__banner-750w' => 0,
          'colorbox__banner-wide' => 0,
          'colorbox__body-500px-wide' => 0,
          'colorbox__field-slideshow-slide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__image_gallery_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__image_gallery_standard' => 0,
          'colorbox__image_gallery_wide' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__uw_service_icon' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_banner-750w' => 0,
          'image_banner-wide' => 0,
          'image_body-500px-wide' => 0,
          'image_field-slideshow-slide' => 0,
          'image_focal_point_preview' => 0,
          'image_image_gallery_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_image_gallery_standard' => 0,
          'image_image_gallery_wide' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_uw_service_icon' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'label_help_description' => '',
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-uw_site_footer_logo_areas-field_uw_show_sf_selection'.
  $field_instances['taxonomy_term-uw_site_footer_logo_areas-field_uw_show_sf_selection'] = array(
    'bundle' => 'uw_site_footer_logo_areas',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Choose if this should be shown in the site footer selection drop down menu when users are selecting a site footer on their site.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_uw_show_sf_selection',
    'label' => 'Show in site footer selection',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
        'label_help_description' => '',
      ),
      'type' => 'options_onoff',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Choose if this should be shown in the site footer selection drop down menu when users are selecting a site footer on their site.');
  t('Enter the link that the logo will go to.');
  t('Faculty/Department/Area');
  t('Logo');
  t('Select the faulty, department or area that the site footer belongs to.');
  t('Show in site footer selection');
  t('link');

  return $field_instances;
}
