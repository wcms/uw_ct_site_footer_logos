<?php

/**
 * @file
 * uw_ct_site_footer_logos.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_site_footer_logos_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'University of Waterloo',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '00d531bb-f0a7-479c-88d7-899f4a3aa232',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Departments and areas',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 8,
    'uuid' => '15e2bfbc-ed42-4912-88d0-d4441ba0bcdd',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Environment',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => '1a701fe4-978e-4887-bd15-05deebb09903',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Colleges and affiliates',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 7,
    'uuid' => '4cbb73aa-74b0-4c25-acbb-666ca30ae85e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 0,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Institutes and centres',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 9,
    'uuid' => '5b25b5de-80f2-4894-a2af-f16c943b5099',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Mathematics',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '5ca7a409-88f6-43ca-8f63-a97e9e66a813',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Engineering',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => 'a3c3ad83-2423-43c8-8a92-a6556abc21ae',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Applied Health Sciences',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => 'a73f3810-4585-4196-b02a-81c26c4f09f5',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Arts',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => 'bc341793-fd95-4c41-8fc6-84f5612591c3',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Science',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 6,
    'uuid' => 'e58eb1ad-1f1c-4694-8cbb-99b798bd3a56',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_site_footer_logo_areas',
    'field_uw_show_sf_selection' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
      'und' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
