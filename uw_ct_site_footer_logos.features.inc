<?php

/**
 * @file
 * uw_ct_site_footer_logos.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_site_footer_logos_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_site_footer_logos_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_site_footer_logos_node_info() {
  $items = array(
    'uw_ct_site_footer_logo' => array(
      'name' => t('Site footer logo'),
      'base' => 'node_content',
      'description' => t('Site footer logos'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
