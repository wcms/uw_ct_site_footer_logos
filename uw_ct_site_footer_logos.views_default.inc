<?php

/**
 * @file
 * uw_ct_site_footer_logos.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_site_footer_logos_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'uw_view_site_footer_logos';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Site footer logos';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Site footer logos';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'uw_site_footer_logo_areas' => 'uw_site_footer_logo_areas',
    'uwaterloo_audience' => 0,
    'bibliography_keywords' => 0,
    'uw_blog_tags' => 0,
    'uwaterloo_contact_group' => 0,
    'uw_event_tags' => 0,
    'uw_event_type' => 0,
    'uwaterloo_profiles' => 0,
    'project_role' => 0,
    'project_status' => 0,
    'project_topic' => 0,
    'service_categories' => 0,
    'uw_service_tags' => 0,
    'strategic_alignment' => 0,
  );
  /* Relationship: Content: Faculty/Department/Area (field_uw_site_footer_area) */
  $handler->display->display_options['relationships']['field_uw_site_footer_area_tid']['id'] = 'field_uw_site_footer_area_tid';
  $handler->display->display_options['relationships']['field_uw_site_footer_area_tid']['table'] = 'field_data_field_uw_site_footer_area';
  $handler->display->display_options['relationships']['field_uw_site_footer_area_tid']['field'] = 'field_uw_site_footer_area_tid';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Logo area */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'term_node_tid';
  $handler->display->display_options['fields']['name']['ui_name'] = 'Logo area';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Content: Logo */
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['id'] = 'field_uw_site_footer_logo';
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['table'] = 'field_data_field_uw_site_footer_logo';
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['field'] = 'field_uw_site_footer_logo';
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['label'] = '';
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_uw_site_footer_logo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: link */
  $handler->display->display_options['fields']['field_uw_site_footer_link']['id'] = 'field_uw_site_footer_link';
  $handler->display->display_options['fields']['field_uw_site_footer_link']['table'] = 'field_data_field_uw_site_footer_link';
  $handler->display->display_options['fields']['field_uw_site_footer_link']['field'] = 'field_uw_site_footer_link';
  $handler->display->display_options['fields']['field_uw_site_footer_link']['label'] = '';
  $handler->display->display_options['fields']['field_uw_site_footer_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_uw_site_footer_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_uw_site_footer_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_uw_site_footer_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_uw_site_footer_link']['type'] = 'link_plain';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="uw-site-footer-logo-view-row-wrapper">
<h2>[title]</h2>
<h3>[name]</h3>
<div class="uw-site-footer-logo-wrapper">
<div class="uw-site-footer-logor">
<a href="[field_uw_site_footer_link] ">
[field_uw_site_footer_logo] 
</a>
</div>
</div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
  $handler->display->display_options['sorts']['weight']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['weight']['expose']['label'] = 'Faculty/Department/Area by weight';
  /* Sort criterion: Taxonomy term: Name */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  $handler->display->display_options['sorts']['name']['relationship'] = 'term_node_tid';
  $handler->display->display_options['sorts']['name']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['name']['expose']['label'] = 'Department/Faculty/Area by Name';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['title']['expose']['label'] = 'Title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'uw_ct_site_footer_logo' => 'uw_ct_site_footer_logo',
  );
  /* Filter criterion: Content: Faculty/Department/Area (field_uw_site_footer_area) */
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['id'] = 'field_uw_site_footer_area_tid';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['table'] = 'field_data_field_uw_site_footer_area';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['field'] = 'field_uw_site_footer_area_tid';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['expose']['operator_id'] = 'field_uw_site_footer_area_tid_op';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['expose']['label'] = 'Faculty/Department/Area';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['expose']['operator'] = 'field_uw_site_footer_area_tid_op';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['expose']['identifier'] = 'field_uw_site_footer_area_tid';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_uw_site_footer_area_tid']['vocabulary'] = 'uw_site_footer_logo_areas';

  /* Display: Site footer logos */
  $handler = $view->new_display('page', 'Site footer logos', 'page');
  $handler->display->display_options['display_description'] = 'Display for site footer logos';
  $handler->display->display_options['path'] = 'site-footer-logos';
  $translatables['uw_view_site_footer_logos'] = array(
    t('Master'),
    t('Site footer logos'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('term'),
    t('term from field_uw_site_footer_area'),
    t('<div class="uw-site-footer-logo-view-row-wrapper">
<h2>[title]</h2>
<h3>[name]</h3>
<div class="uw-site-footer-logo-wrapper">
<div class="uw-site-footer-logor">
<a href="[field_uw_site_footer_link] ">
[field_uw_site_footer_logo] 
</a>
</div>
</div>
</div>'),
    t('Faculty/Department/Area by weight'),
    t('Department/Faculty/Area by Name'),
    t('Title'),
    t('Faculty/Department/Area'),
    t('Display for site footer logos'),
  );
  $export['uw_view_site_footer_logos'] = $view;

  return $export;
}
